
const SC = {}

SC.check_order_log = (data) => {
    const { change, id, staff_id } = data;

    if (change.substatus) {
        const { substatus } = change;

        return {
            order_id: id,
            action: substatus,
            staff_id
        };
    }
    else if (change.food_status) {
        const { food_status } = change;
        if (food_status === "ready") {
            return {
                order_id: id,
                action: "kitchen",
                staff_id
            };
        }
        else return null;
    }
    else return null;
};

SC.check_table_update = (data) => {
    if (data.change.substatus) {
        const { change: { substatus }, place_id, table_no } = data;
        if (substatus === "review" || substatus === "reported" || substatus === "canceled") {
            return {
                status: "free",
                place_id,
                table_no
            };
        }
        else {
            return null;
        };
    }
    else {
        return null;
    };
};

module.exports = SC;