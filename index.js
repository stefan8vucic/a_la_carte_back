const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});

const bodyParser = require("body-parser");
const cors = require("cors");
const db = require("./db/db_services");
const auth_db = require("./db/auth_db_services");
const jwt = require("jsonwebtoken");
const SC = require("./socket_check");


// ROUTES
const user_route = require("./routes/users");
const places_route = require("./routes/places");
const staff_route = require("./routes/staff");
const auth_route = require("./routes/auth");

const PORT = process.env.PORT || 2302;

app.use(bodyParser.json());
app.use(cors());
app.use("/api/users", user_route);
app.use("/api/places", places_route);
app.use("/api/staff", staff_route);
app.use("/api/auth", auth_route);

io.use((socket, next) => {
  console.log("aaa")
  next();
  // if (isValid(socket.request)) {
  //   console.log("aaa")
  //   next();
  // } else {
  //   console.log("bbb")
  //   next(new Error("invalid"));
  // }
})
  .on("connection", async (socket) => {
    // if (socket.handshake.query.type === "place") {
    const { token } = socket.handshake.query;
    console.log("desila se konekcija");
    let handler = { ...socket.handshake.query }
    try {
      handler.socket_id = socket.id
      await auth_db.update_place_session(handler);
    }
    catch (err) {
      console.log(err);
    };
    // }

    socket.on("new_order", async (data) => {
      try {
        const order_id = await db.handle_new_order(data);

        data.id = Number(order_id);
        // data.creation = new Date().getTime() + new Date().getTimezoneOffset() * 60 * 1000;

        const response = await auth_db.get_place_sockets(data.place_id);
        if (response.length) {
          response.forEach(item => io.to(item.socket_id).emit("order", {
            event: "new",
            data
          }));
        };
      }
      catch (err) {
        socket.emit("order", {
          event: "error",
          data
        });
      }
    });

    socket.on("join_order", async (data) => {
      try {
        // throw new Error();
        await db.join_order(data);
        const response = await auth_db.get_place_sockets(data.place_id);
        if (response.length) {
          response.forEach(item => io.to(item.socket_id).emit("order", {
            event: "join",
            data
          }));
        };
      }
      catch (err) {
        socket.emit("order", {
          event: "error",
          data
        });
      };
    });



    socket.on("change_order_status", async (data) => {
      console.log(data)
      try {
        // throw new Error();
        // vraca placeID i userID i table_no za narudzbu da mozemo traziti socketID i table status
        await db.handle_order_status_update(data);

        // const table_update_check = SC.check_table_update(data);
        // if (table_update_check) {
        //   await db.update_table_status(table_update_check);
        // };

        // const order_log_check = SC.check_order_log(data);
        // if (order_log_check) {
        //   await db.create_order_log(order_log_check);
        // };
        console.log(data.place_id)
        const response = await auth_db.get_place_sockets(data.place_id);
        if (response.length) {
          response.forEach(item => io.to(item.socket_id).emit("order", {
            event: "update",
            data
          }));
        };
      }
      catch (err) {
        socket.emit("order", {
          event: "error",
          data
        });
      };
    });


    socket.on("change_order_table", async (data) => {
      try {

         await db.handle_order_table_change(data);
        
        // console.log(aaa)
        const response = await auth_db.get_place_sockets(data.place_id);
        if (response.length) {
          response.forEach(item => io.to(item.socket_id).emit("order", {
            event: "table",
            data
          }));
        };
      }
      catch (err) {
        socket.emit("order", {
          event: "error",
          data
        });
      };
    });



    socket.on("reconnection", async () => {
      console.log("rekonekcija")
      try {
        handler.socket_id = socket.id
        await auth_db.update_place_session(handler);
      }
      catch (err) {
        console.log(err);
      };
    });

    socket.on("disconnect", async () => {
      console.log("disconnected");
      // await auth_db.delete_place_session(token);
    });
  });

 
//   socket.on("bill_please", async (m) => {
//     console.log(m);
//     try {
//       const rest = await db.check_r_session(m.rest_id);
//       socket.to(rest[0].r_socket_id).emit("bill_please", m.order_id);
//       // socket.emit("bill_please", {id: m.order_id})
//     }
//     catch (err) {
//       console.log(err);
//     };
//   });




//   socket.on("cancel_order", async (data) => {
//     console.log(data)
//     try {
//       const rest = await db.check_r_session(data.rest_id);
//       socket.to(rest[0].r_socket_id).emit("cancel_order", data.order_id);
//     }
//     catch (err) {
//       console.log(err);
//     };
//   });


server.listen(PORT, () => console.log(`Server ok! and running on ${PORT}`));





// db.delete_table()

// db.create_table()

// db.update_reservation_msg({id: 1000011, sender: 12345, message: "Hvala. Vidimo se." })


const tables = [
  [1, 100003, 4, "Gornja terasa", "free"],
  [2, 100003, 4, "Gornja terasa", "free"],
  [3, 100003, 4, "Gornja terasa", "free"],
  [4, 100003, 4, "Gornja terasa", "free"],
  [5, 100003, 4, "Gornja terasa", "free"],
  [6, 100003, 4, "Gornja terasa", "free"]
];


// db.insert_multiple(tables);

// db.update_in_table()

const bcrypt = require("bcryptjs");

const place_login = {
  place_id: 100001,
  user_name: "user3",
  password: "user3",
  pos: false,
  kitchen: true,
  staff: true,
  events: true
}

const asas = async (req) => {
  try {
    const salt = await bcrypt.genSalt(10);
    const hashed_pass = await bcrypt.hash(req.password, salt);
    req.password = hashed_pass;
    await auth_db.place_registration(req);

  }
  catch (err) {
    console.log(err);

  };
};

// asas(place_login)





let aaa = {
  join_id: 100001,
  join_to_id: "100011",
  total: 5
}

// db.join_order(aaa)


// auth_db.delete_table()

// db.delete_item();

// auth_db.create_table()

// auth_db.delete_place_session()












const dodaj_restoran_postman = {
  "admin_id": 100001,
  "email": "akademik@restaurant.com",
  "name": "Akademik",
  "phone": "+38766063374",
  "city": "Banja Luka",
  "country": "Bosnia and Herzegovina",
  "zip": 78000,
  "address": "Address1",
  "language": "srp",
  "type": "Bar, Restaurant",
  "description": "Najbolja zabava svako vece, uz veoma povoljnu cijenu hrane i pica",
  "reservations": "yes",
  "currency": "BAM",
  "priority": "low",
  "facebook": "https://www.facebook.com/%D0%9A%D0%B0%D1%84%D0%B5-%D0%B1%D0%B0%D1%80-%D0%90%D0%BA%D0%B0%D0%B4%D0%B5%D0%BC%D0%B8%D0%BA-1716762081951964/",
  "instagram": "https://www.instagram.com/kafebarakademik/",
  "web": "",
  "viber": "38766063374",
  "wa": "38766063374",
  "orders": "closed",
  "kitchen": "closed",
  "logo": "https://scontent-cdg2-1.xx.fbcdn.net/v/t31.0-8/22792155_1736991493262356_8716428809636919353_o.jpg?_nc_cat=100&ccb=3&_nc_sid=e3f864&_nc_ohc=bGW7R1aHv3AAX-M9qUQ&_nc_ht=scontent-cdg2-1.xx&oh=4e3f146782b20e729ed2e4aa1cad6470&oe=605C4D58"
}


// db.place_registration(dodaj_restoran_postman);


const menu2 = [

  [[100001, "Tea", "", "200ml", "The best black tea", 1.5, "drinks", "hot_drinks", "tea", "", true]],
  [[100001, "Coca-Cola", "", "250ml", "", 2.5, "drinks", "softs", "carbonated", "", true]],
  [[100001, "Fanta", "", "250ml", "", 2.5, "drinks", "softs", "carbonated", "", true]],
  [[100001, "Squeezed Orange", "", "200ml", "Freshly squeezed orange juice", 3, "drinks", "softs", "squeezed_juice", "", true]],
  [[100001, "Vegetable soup", "", "portion", "Finest homemade, vegetable mix soup", 3, "food", "soup", "", "", true]],
  [[100001, "Chicken soup", "", "portion", "Finest homemade, chicken and vegetable mix soup", 4, "food", "soup", "", "", true]],
  [[100001, "Beef burger", "", "portion", "150g of finest beef + French fries", 6, "food", "burger", "beef", "", true]],
  [[100001, "Chicken nuggets", "", "portion", "250g of finest chicken + French fries", 6, "food", "nugets", "chicken", "", true]],
  [[100001, "Nektar", "", "0.33l", "Mali Nektar najbolji na svijetu", 2.5, "drinks", "Beers", "Nektar", "", true]],
  [[100001, "Nektar", "", "0.50l", "Veliki Nektar najbolji na svijetu", 2.0, "drinks", "Beers", "Nektar", "", true]],
  [[100001, "Tuborg", "", "0.50l", "Veliki Tuborg", 2.5, "drinks", "Beers", "Tuborg", "", true]],
  [[100001, "Jelen", "", "0.50l", "Veliki Jelen", 2.0, "drinks", "Beers", "Jelen", "", true]],
  [[100001, "Jelen", "", "0.33l", "Mali Jelen", 2.5, "drinks", "Beers", "Jelen", "", true]],
  [[100001, "Bavaria", "", "0.25l", "Mala Bavaria", 2.5, "drinks", "Beers", "Bavaria", "", true]]
]

const menu3 = [
  [[100001, "Nektar", "", "0.33l", "Mali Nektar najbolji na svijetu", 2.5, "drinks", "Beers", "Nektar", "", true]],
  [[100001, "Nektar", "", "0.50l", "Veliki Nektar najbolji na svijetu", 2.0, "drinks", "Beers", "Nektar", "", true]],
  [[100001, "Jelen", "", "0.50l", "Veliki Jelen", 2.0, "drinks", "Beers", "Jelen", "", true]],
  [[100001, "Bavaria", "", "0.25l", "Mala Bavaria", 2.5, "drinks", "Beers", "Bavaria", "", true]]
]

// menu3.forEach(item => db.update_menu(item))

// db.update_menu([ [100001, "Coffee", "", "20ml", "The best Brazilian coffee", 1.5, "drinks", "hot_drinks", "coffee", "", true]])
