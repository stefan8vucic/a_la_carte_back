const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});

const bodyParser = require("body-parser");
const cors = require("cors");
const db = require("./db/db_services");
const jwt = require("jsonwebtoken");


// ROUTES
const user_route = require("./routes/users");
const restaurants_route = require("./routes/places");

const PORT = process.env.PORT || 2302;

app.use(bodyParser.json());
app.use(cors());
app.use("/api/users", user_route);
app.use("/api/restaurants", restaurants_route);

io.on("connection", async (socket) => {
  console.log("desila se konekcija")
  try {
    const token = socket.handshake.query.ime;
    if (token) {
      // socket.broadcast.emit("order-confirmed", "JEST")
      const dekod = jwt.decode(socket.handshake.query.ime, process.env.AUTH_TOCKEN_SECRET);
      const user = await db.check_r_session(dekod);
      if (user.length) {
        const data = {
          r_socket_id: socket.id,
          r_id: dekod
        }
        await db.update_r_session(data)
      }
      else {
        console.log(token)
        const data = {
          r_socket_id: socket.id,
          r_id: dekod,
          r_ref_token: token
        }
        await db.create_r_session(data)
      }
    }
    else {
      console.log("nema tokena")
    }
  }
  catch (err) {
    console.log(err);
  }

  socket.on("reconnection", () => {
    console.log("reconect")
  });


  socket.on("changed_status", () => {
    socket.broadcast.emit("changed_status");
  });

  socket.on("bill_please", async (m) => {
    console.log(m);
    try {
      const rest = await db.check_r_session(m.rest_id);
      socket.to(rest[0].r_socket_id).emit("bill_please", m.order_id);
      // socket.emit("bill_please", {id: m.order_id})
    }
    catch (err) {
      console.log(err);
    };
  });


  socket.on("order", async (m) => {
    try {
      const rest = await db.check_r_session(m.rest_id);
      socket.to(rest[0].r_socket_id).emit("confirmation", m);
      socket.emit("confirmation", m)
    }
    catch (err) {
      console.log(err);
    };
  });

  socket.on("cancel_order", async (data) => {
    console.log(data)
    try {
      const rest = await db.check_r_session(data.rest_id);
      socket.to(rest[0].r_socket_id).emit("cancel_order", data.order_id);
    }
    catch (err) {
      console.log(err);
    };
  });

  socket.on("disconnect", async () => {
    console.log("disconnected")
    try {
      if (socket.handshake.query.ime) {
        const dekod2 = jwt.decode(socket.handshake.query.ime, process.env.AUTH_TOCKEN_SECRET);
        console.log(dekod2, "ime");
        await db.delete_r_session(dekod2);
      }
      else {
        console.log(socket.handshake.query.rest)
        const rest = await db.check_r_session(socket.handshake.query.rest);
        socket.to(rest[0].r_socket_id).emit("visit_stoped");
        console.log("rest")
      }
    }
    catch (err) {
      console.log(err);
    };
  });

  socket.on("new_visit", async (id) => {
    console.log("new_visit", id)
    try {
      const rest = await db.check_r_session(id);
      console.log(rest[0])
      socket.to(rest[0].r_socket_id).emit("new_visit");
    }
    catch (err) {
      console.log(err);
    };
  });

});

server.listen(PORT, () => console.log(`Server ok! and running on ${PORT}`));

// db.delete_table()

// db.create_table()

// db.update_reservation_msg({id: 1000011, sender: 12345, message: "Hvala. Vidimo se." })

const b = {
  r_socket_id: 123,
  r_id: 100101
}

// db.delete_item()

// db.update_r_session(b)
// db.delete_r_session()

// db.delete_r_session(1000031);

const tables = [
  [1, 1000011, 4, "free"],
  [2, 1000011, 4, "free"],
  [3, 1000011, 4, "free"],
  [4, 1000011, 4, "free"],
  [5, 1000011, 4, "free"],
]

// db.insert_multiple()

const menu3 = [
  [1000011, "Nektar", "", "0.33l", "Mali Nektar najbolji na svijetu", 2.5, "drinks", "Beers", "Nektar"],
  [1000011, "Nektar", "", "0.50l", "Veliki Nektar najbolji na svijetu", 2.0, "drinks", "Beers", "Nektar"],
  [1000011, "Tuborg", "", "0.50l", "Veliki Tuborg", 2.5, "drinks", "Beers", "Tuborg"],
  [1000011, "Jelen", "", "0.50l", "Veliki Jelen", 2.0, "drinks", "Beers", "Jelen"],
  [1000011, "Jelen", "", "0.33l", "Mali Jelen", 2.5, "drinks", "Beers", "Jelen"],
  [1000011, "Bavaria", "", "0.25l", "Mala Bavaria", 2.5, "drinks", "Beers", "Bavaria"]
]

// db.update_menu(menu3)


// const aa = {
//     user_name: "Krupa.sp",
//     user_password: 1234,
//     zip: 78000
// }

// db.post(aa)