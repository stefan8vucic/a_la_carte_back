const express = require("express");
const router = express.Router();
const db = require("../db/db_services");
const auth_db = require("../db/auth_db_services");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const verify = require("./token_verification");


router.post("/login", async (req, res) => {
    const { user_name, password, type } = req.body;
    try {
        let account = await auth_db.check_place_login(user_name);
        if (!account.length) return res.status(401).json({ message: "Not found" });
        if (req.body.user_name !== account[0].user_name) return res.status(401).json({ message: "Not found" });

        const password_check = await bcrypt.compare(password, account[0].password);
        if (!password_check) return res.status(401).json({ message: "Not found" });

        // if (account[0][type].lastIndexOf(1) === -1) return res.status(401).json({ message: "Not found" });

        const session_id = await auth_db.create_session_log(account[0].place_id, account[0].id);

        // update_orders true
        const access_token = create_jwt(account[0].id, account[0].place_id, session_id, "");
        const ref_token = create_jwt(account[0].id, account[0].place_id, session_id, process.env.REFRESH_TOCKEN_SECRET);

        account[0].password = null;
        account[0].a_token = access_token;
        account[0].r_token = ref_token;
        account[0].session_id = session_id;

        await auth_db.create_session(account[0].place_id, ref_token);

        res.json(account[0]);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/session_check", async (req, res) => {
    const r_token = req.header('a_la_rt');
    try {
        await auth_db.session_check(r_token);
        const data = jwt.verify(r_token, process.env.REFRESH_TOCKEN_SECRET);

        const access_token = create_jwt(data.id, data.place_id, data.session_id, "");
        const ref_token = create_jwt(data.id, data.place_id, data.session_id, process.env.REFRESH_TOCKEN_SECRET);

        await auth_db.update_session_token(r_token, ref_token);

        data.a_token = access_token;
        data.r_token = ref_token;

        res.json(data);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.post("/logout", async (req, res) => {
    console.log("logout")
    try {
        await auth_db.delete_place_session(req.body.token);
        await auth_db.update_session_log(req.body);
        res.send("OK");
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});



const create_jwt = (id, place_id, session_id, ref_token) => {
    if (!ref_token) {
        return jwt.sign({ id, place_id, session_id }, process.env.AUTH_TOCKEN_SECRET, {
            expiresIn: '30m'
        });
    }
    else {
        return jwt.sign({ id, place_id, session_id }, ref_token);
    };
};

module.exports = router;