const express = require("express");
const router = express.Router();
const db = require("../db/db_services");
const auth_db = require("../db/auth_db_services");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const table_profile = "restaurants_profile";

router.get("/", async (req, res) => {
    console.log(req.query)
    try {
        const restaurants = await db.get_filtered_rest(table_profile, req.query);
        res.json(restaurants);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/filterdata", async (req, res) => {
    console.log(req.query)
    try {
        const restaurant = await db.get_filter_data(req.query.city);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/profile/:id", async (req, res) => {
    try {
        const restaurant = await db.get_place(req.params.id);
        if (restaurant.length) {
            res.json(restaurant);
        }
        else {
            return res.status(404).json({ message: "Place not found" });
        };
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/session/:id", async (req, res) => {
    try {
        const restaurant = await db.check_r_session(req.params.id);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});





router.post("/menu", async (req, res) => {

    console.log(req.body)
    try {
        const restaurant = await db.update_menu(req.body);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

// router.post("/registration", async (req, res) => {
//     try {
//         const salt = await bcrypt.genSalt(10);
//         const hashed_pass = await bcrypt.hash(req.body.password, salt);
//         req.body.password = hashed_pass;
//         const login_inserted = await db.registration(req.body, table_login);
//         req.body.id = login_inserted.insertId;
//         const profile_inserted = await db.registration(req.body, table_profile);
//         // const inserted_restaurant = await db.get_one(inserted.insertId, table);
//         // res.json(inserted_restaurant);
//     }
//     catch (err) {
//         console.log(err);
//         res.sendStatus(500);
//     };
// });





router.post("/orders", async (req, res) => {
    console.log(req.body)
    try {
        const restaurant = await db.update_order(req.body);
        res.json(restaurant.insertId);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.patch("/orders", async (req, res) => {
    console.log(req.body)
    try {
        const restaurant = await db.handle_order_status_update(req.body);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});




router.post("/order-items", async (req, res) => {
    console.log(req.body)
    try {
        const restaurant = await db.update_order_items(req.body);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/visit/:id", async (req, res) => {
    console.log(req.params.id);
    try {
        const restaurant = await db.get_visits_count(req.params.id);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.post("/visit", async (req, res) => {
    console.log("visit", req.body)
    try {
        const restaurant = await db.update_visit(req.body);
        res.json(restaurant.insertId);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/tables/:id", async (req, res) => {
    try {
        const restaurant = await db.get_free_tables(req.params.id);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.patch("/tables", async (req, res) => {
    try {
        const restaurant = await db.update_table_status(req.body);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/events", async (req, res) => {
    console.log(req.query)
    try {
        const restaurant = await db.get_events(req.query);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.post("/evaluation", async (req, res) => {
    console.log("evaluation", req.body)
    try {
        const restaurant = await db.update_evaluation(req.body);
        res.json(restaurant.insertId);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/evaluation", async (req, res) => {
    try {
        const restaurant = await db.get_evaluations(req.query);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.post("/reservations", async (req, res) => {
    try {
        const restaurant = await db.update_reservation(req.body);
        res.json(restaurant.insertId);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.post("/reservations_msg", async (req, res) => {
    try {
        const restaurant = await db.update_reservation_msg(req.body);
        res.json(restaurant.insertId);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/reservations", async (req, res) => {
    try {
        const restaurant = await db.get_reservations(req.query);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});


// NOVO

router.post("/place_registration", async (req, res) => {
    try {
        const response = await db.place_registration(req.body);
        res.json(response);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.post("/session_registration", async (req, res) => {
    try {
        const salt = await bcrypt.genSalt(10);
        const hashed_pass = await bcrypt.hash(req.body.password, salt);
        req.body.password = hashed_pass;
        const response = await auth_db.place_registration(req.body);
        res.json(response);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});










router.get("/menu/:id", async (req, res) => {

    console.log(req.params.id, req.query)
    try {
        const restaurant = await db.get_menu(req.params.id, req.query);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.patch("/menu", async (req, res) => {
    
    try {
        await db.update_menu_item(req.body);
        res.json(req.body);
        // res.sendStatus(400);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/place_status/:id", async (req, res) => {
    try {
        const restaurant = await db.get_place_status(req.params.id);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.patch("/place_status", async (req, res) => {
    try {
        await db.update_place_status(req.body);
        res.json(req.body);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

const a 


router.get("/menu_search", async (req, res) => {
    try {
        const restaurant = await db.menu_search_item(req.query);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

router.get("/orders/:id", async (req, res) => {
    try {
        const restaurant = await db.get_active_orders(req.params.id);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});


router.get("/sessionss", async (req, res) => {
    try {
        const restaurant = await auth_db.get_session_logs();
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});





module.exports = router;
