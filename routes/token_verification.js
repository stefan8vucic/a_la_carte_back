const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
    const a_token = req.header('a_la_at');
    const place_id = req.params.id;
    if (a_token) {
        jwt.verify(a_token, process.env.AUTH_TOCKEN_SECRET, (err, verify) => {
            if (err) return res.status(401).send("Unvalid token");

            if(+verify.place_id !== +place_id) return res.status(401).send("Unvalid token"); 


            // req.verify_user_name = verify.user_name;
            // req.verify_id = verify.id;
            next();
            // console.log(+verify.place_id === +place_id);
        });
    }
    else {
        return res.status(401).send("Unvalid token");
    };
};