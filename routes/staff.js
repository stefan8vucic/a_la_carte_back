const express = require("express");
const router = express.Router();
const db = require("../db/db_services");
const auth_db = require("../db/auth_db_services");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const verify = require("./token_verification");

router.get("/data/:id", verify, async (req, res) => {
    try {
        const restaurant = await db.handle_staff_initial_data_load(req.params.id);
        res.json(restaurant);
    }
    catch (err) {
        console.log(err);
        res.sendStatus(500);
    };
});

module.exports = router;