
const utils = {}

utils.check_order_log = (data) => {
    const { change } = data;

    if (change.substatus) {
        const { substatus } = change;
        return substatus;
    }
    else if (change.food_status) {
        const { food_status } = change;
        if (food_status === "ready") {
            return "kitchen" 
        }
        else return null;
    }
    else if (change.table_no) {
        return "table"
    }
    else return null;
};

utils.check_table_update = (data) => {
    if (data.change.substatus) {
        const { substatus } = data.change;
        if (substatus === "review" || substatus === "reported" || substatus === "canceled") {
            return true;
        }
        else {
            return null;
        };
    }
    else if (data.change.table_no) {
        return true;
    }
    else {
        return null;
    };
};

utils.update_order_table = (data) => {
    const { prev_table, new_table, orders, staff_id } = data;
    const order_log_action = `From table ${prev_table} to ${new_table}`
    let values = [];
    orders.forEach(order => values = [...values, [order, staff_id, order_log_action]]);

    return values;
}

module.exports = utils;




































const dodaj_restoran_postman = {
    "admin_id" : 100001,
    "email" : "akademik@restaurant.com",
    "name" : "Akademik", 
    "phone" : "+38766063374", 
    "city" : "Banja Luka", 
    "country" : "Bosnia and Herzegovina", 
    "zip" : 78000, 
    "address" : "Address1", 
    "type" : "Bar, Restaurant", 
    "description" : "Najbolja zabava svako vece, uz veoma povoljnu cijenu hrane i pica", 
    "reservations" : "yes", 
    "currency" : "BAM", 
    "priority": "low",
    "facebook" : "https://www.facebook.com/%D0%9A%D0%B0%D1%84%D0%B5-%D0%B1%D0%B0%D1%80-%D0%90%D0%BA%D0%B0%D0%B4%D0%B5%D0%BC%D0%B8%D0%BA-1716762081951964/",
    "instagram" : "https://www.instagram.com/kafebarakademik/",
    "web" : "",
    "viber" : "38766063374",
    "wa" : "38766063374",
    "status" : "close",
    "kitchen" : "close",
    "logo" : "https://scontent-cdg2-1.xx.fbcdn.net/v/t31.0-8/22792155_1736991493262356_8716428809636919353_o.jpg?_nc_cat=100&ccb=3&_nc_sid=e3f864&_nc_ohc=bGW7R1aHv3AAX-M9qUQ&_nc_ht=scontent-cdg2-1.xx&oh=4e3f146782b20e729ed2e4aa1cad6470&oe=605C4D58"
}

const menu = [
    [1000011, "Coffe", "", "Brasilien coffee, the best", 1.5, "drinks", "hot_drinks"], 
    [1000011, "Tea", "", "Black tea, the best", 1.5, "drinks", "hot_drinks"], 
]

const menu2 = [
    [100001, "Coffee", "", "20ml", "The best Brazilian coffee", 1.5, "drinks", "hot_drinks", "coffee", "", true], 
    [100001, "Tea", "", "200ml", "The best black tea", 1.5, "drinks", "hot_drinks", "tea", "", true],
    [100001, "Coca-Cola", "", "250ml", "", 2.5, "drinks", "softs", "carbonated", "", true], 
    [100001, "Fanta", "", "250ml", "", 2.5, "drinks", "softs", "carbonated", "", true], 
    [100001, "Squeezed Orange", "", "200ml", "Freshly squeezed orange juice", 3, "drinks", "softs", "squeezed_juice", "", true], 
    [100001, "Vegetable soup", "", "portion", "Finest homemade, vegetable mix soup", 3, "food", "soup", "", "", true], 
    [100001, "Chicken soup", "", "portion", "Finest homemade, chicken and vegetable mix soup", 4, "food", "soup", "", "", true], 
    [100001, "Beef burger", "", "portion", "150g of finest beef + French fries", 6, "food", "burger", "beef", "", true], 
    [100001, "Chicken nuggets", "", "portion", "250g of finest chicken + French fries", 6, "food", "nugets", "chicken", "", true] 
    [100001, "Nektar", "", "0.33l", "Mali Nektar najbolji na svijetu", 2.5, "drinks", "Beers", "Nektar", "", true], 
    [100001, "Nektar", "", "0.50l", "Veliki Nektar najbolji na svijetu", 2.0, "drinks", "Beers", "Nektar", "", true],
    [100001, "Tuborg", "", "0.50l", "Veliki Tuborg", 2.5, "drinks", "Beers", "Tuborg", "", true], 
    [100001, "Jelen", "", "0.50l", "Veliki Jelen", 2.0, "drinks", "Beers", "Jelen", "", true],
    [100001, "Jelen", "", "0.33l", "Mali Jelen", 2.5, "drinks", "Beers", "Jelen", "", true],
    [100001, "Bavaria", "", "0.25l", "Mala Bavaria", 2.5, "drinks", "Beers", "Bavaria", "", true]
]

const tables = [
    [0, 100001, 4, "Sank", "free"],
    [1, 100001, 4, "Gornja terasa", "free"],
    [2, 100001, 4, "Gornja terasa", "free"],
    [3, 100001, 4, "Gornja terasa", "free"],
    [4, 100001, 4, "Gornja terasa", "free"],
    [5, 100001, 4, "Gornja terasa", "free"],
    [6, 100001, 4, "Gornja terasa", "free"],
    [7, 100001, 4, "Srednja terasa", "free"],
    [8, 100001, 4, "Srednja terasa", "free"],
    [9, 100001, 4, "Srednja terasa", "free"],
    [10, 100001, 4, "Srednja terasa", "free"],
    [11, 100001, 4, "Srednja terasa", "free"],
    [12, 100001, 4, "Srednja terasa", "free"],
    [13, 100001, 4, "Donja terasa", "free"],
    [14, 100001, 4, "Donja terasa", "free"],
    [15, 100001, 4, "Donja terasa", "free"],
    [16, 100001, 4, "Donja terasa", "free"],
    [17, 100001, 4, "Donja terasa", "free"],
    [18, 100001, 4, "Donja terasa", "free"],
    [19, 100001, 4, "Donja terasa", "free"],
    [20, 100001, 4, "Donja terasa", "free"],
  ]

const tables_query = `INSERT INTO tables (table_no, place_id, place, status) VALUES ?`

const events = [
    ["https://cdnimg.webstaurantstore.com/uploads/blog/2016/4/acoustic-artist-live-in-bar.jpg", "1000011", "top", "Top Band - Live", "", "music", "", "2021-02-20 20:00:00"], 
    ["https://media-cdn.tripadvisor.com/media/photo-s/08/f2/67/f1/fat-mo-s.jpg", "1000031", "mid", "Extra Band - Live & Paulaner promotion", "Paulaner 0.5l price for the night 3KM", "music", "promotion", "2021-02-20 20:30:00"],
    ["https://www.corkcityfc.ie/home/wp-content/uploads/2018/05/UEFA-Champions-League.jpg", "1000011", "low", "Champions League - Final", "All snacks -20%", "sport", "promotion", "2021-02-21 20:00:00"], 
    ["https://media.timeout.com/images/101619085/630/472/image.jpg", "1000031", "mid", "Table Football Tournament", "Winner gets all drinks free", "sport", "", "2021-02-21 16:00:00"], 
    ["https://image.freepik.com/free-vector/karaoke-neon-signs-style_118419-546.jpg", "1000011", "mid", "Karaoke Tournament", "Winner gets all drinks free", "sport", "music", "2021-02-21 21:00:00"],
    ["https://www.theiwsr.com/wp-content/uploads/Wine-Roundup-700x525px-600x450.jpg", "1000031", "top", '"The Best Wine" promotion', 'All "The Best Wine" products -30% whole week', "promotion", "", "2021-02-22 12:00:00"] 
]

const events_query = `INSERT INTO events (img, rest_id, priority, title, description, type_1, type_2, start) VALUES ?`

const user = {
    "user_name" : "User2",
    "password" : "User2",
    "email" : "user2@mymail.com",
    "phone" : "+387646677889",
    "city" : "Banja Luka",
    "country" : "Bosnia and Herzegovina",
    "zip" : "78000",
    "address" : "User2 address 1",
    "birthday": "2006-2-23",
    "sex" : "f",
    "language" : "eng"
}

const menu3 = [
    [1000011, "Nektar", "", "0.33l", "Mali Nektar najbolji na svijetu", 2.5, "drinks", "Beers", "Nektar"], 
    [1000011, "Nektar", "", "0.50l", "Veliki Nektar najbolji na svijetu", 2.0, "drinks", "Beers", "Nektar"],
    [1000011, "Tuborg", "", "0.50l", "Veliki Tuborg", 2.5, "drinks", "Beers", "Tuborg"], 
    [1000011, "Jelen", "", "0.50l", "Veliki Jelen", 2.0, "drinks", "Beers", "Jelen"],
    [1000011, "Jelen", "", "0.33l", "Mali Jelen", 2.5, "drinks", "Beers", "Jelen"],
    [1000011, "Bavaria", "", "0.25l", "Mala Bavaria", 2.5, "drinks", "Beers", "Bavaria"]
]