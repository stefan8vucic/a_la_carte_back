const mySql = require("mysql");
const { resolve } = require("path");
const { env } = require("process");
require("dotenv/config");
const poolTransaction = require('./pool_transaction');
const QH = require("./query_handler");
const utils = require("./utils");

const pool = mySql.createPool({
    connectionLimit: 100,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    database: process.env.DB_DB,
    port: 3306
});


const create_tables_eva = "CREATE TABLE evaluation ( id INT AUTO_INCREMENT PRIMARY KEY, user_id INT, rest_id INT, order_id INT, menu INT, service INT, ambience INT, comment VARCHAR(300), creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=1000000";

const create_events = "CREATE TABLE events ( id INT AUTO_INCREMENT PRIMARY KEY, img VARCHAR(255), rest_id INT, priority VARCHAR(10), title VARCHAR(255), description VARCHAR(255), type_1 VARCHAR(55), type_2 VARCHAR(55), start TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=1000000";

const create_reservations = "CREATE TABLE reservations ( id INT AUTO_INCREMENT PRIMARY KEY, rest_id INT, user_id INT, persons INT, event_id INT, status VARCHAR(12), substatus VARCHAR(12), start TIMESTAMP, creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP ) ENGINE=InnoDB AUTO_INCREMENT=1000005";

const create_reservations_messages = "CREATE TABLE reservations_msg ( id INT AUTO_INCREMENT PRIMARY KEY, reservation_id INT, sender INT, status VARCHAR(12), message VARCHAR(300), creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP ) ENGINE=InnoDB AUTO_INCREMENT=1000005";

const create_user_login = "CREATE TABLE user_profile ( id INT AUTO_INCREMENT PRIMARY KEY, user_name VARCHAR(20), password VARCHAR(200), email VARCHAR(100), phone VARCHAR(50), birthday DATE, city VARCHAR(50), zip INT, address VARCHAR(255), country VARCHAR(50), sex VARCHAR(5), language VARCHAR(50), last_login TIMESTAMP, creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP ) ENGINE=InnoDB AUTO_INCREMENT=2000000";

const user_query = `INSERT INTO user_profile (user_name, password, email, phone, birthday, city, country, sex, language) VALUES ?`


// NOVO

const create_menu_table = "CREATE TABLE menu (item_id INT AUTO_INCREMENT PRIMARY KEY, place_id INT, name VARCHAR(255), img VARCHAR(1000), volume VARCHAR(55),  description VARCHAR(255), price DECIMAL(6,2), type VARCHAR(255), category VARCHAR(10), subcategory VARCHAR(50), subsubcategory VARCHAR(50), active BIT, last_modification TIMESTAMP, creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP ) ENGINE=InnoDB AUTO_INCREMENT=100000"

const create_orders_table = "CREATE TABLE orders (id INT AUTO_INCREMENT PRIMARY KEY, user_id INT, place_id INT, table_no INT, status VARCHAR(20), substatus VARCHAR(20), food_status VARCHAR(20), message VARCHAR(255), total DECIMAL(7,2), creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=100000"

const create_order_list_table = "CREATE TABLE order_items ( order_id INT, item_id INT, quantity INT, price DECIMAL(6,2))"

const create_visits_table = "CREATE TABLE visits ( user_id INT, place_id INT, creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP)"

const create_tables = "CREATE TABLE tables ( id INT AUTO_INCREMENT PRIMARY KEY, table_no INT, place_id INT, place INT, status VARCHAR(55), sector VARCHAR(55), creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=1000000";

const create_hours = "CREATE TABLE hours ( place_id INT, day INT, open INT, close INT)";

const create_restaurants_profile_table = "CREATE TABLE places ( id INT AUTO_INCREMENT PRIMARY KEY, admin_id INT, name VARCHAR(50), email VARCHAR(100), phone VARCHAR(50), phone2 VARCHAR(50), city VARCHAR(60), country VARCHAR(60), zip INT, address VARCHAR(255), language VARCHAR(6), type VARCHAR(50), description VARCHAR(1000), reservations VARCHAR(10), currency VARCHAR(10), logo VARCHAR(1000), priority VARCHAR(10), facebook VARCHAR(200), instagram VARCHAR(200), web VARCHAR(200), viber VARCHAR(50), wa VARCHAR(50), orders VARCHAR(10), kitchen VARCHAR(10), creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=100000"

const create_place_order_log = "CREATE TABLE order_log (order_id INT, staff_id INT, action VARCHAR(15), creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=100000"


const delete_table = "DROP TABLE menu"

const db = {}


db.delete_table = () => {
    return new Promise((resolve, reject) => {
        pool.query(delete_table, (err, res) => {
            if (err) return reject(err);
            console.log("obrisano")
            return resolve(res);
        });
    });
};

db.create_table = () => {
    return new Promise((resolve, reject) => {
        pool.query(create_menu_table, (err, res) => {
            if (err) return reject(err);
            console.log("kreirano")
            return resolve(res);
        });
    });
};

// NOVO

// PLACES

// registrujemo novi objekat

db.place_registration = (data) => {
    let values = [[data.admin_id, data.name, data.email, data.phone, data.phone2, data.city, data.country, data.zip, data.address, data.language, data.type, data.description, data.reservations, data.currency, data.priority, data.facebook, data.instagram, data.web, data.viber, data.wa, data.orders, data.kitchen, data.logo]];
    return new Promise((resolve, reject) => {
        pool.query(
            `INSERT INTO places (admin_id, name, email, phone, phone2, city, country, zip, address, language, type, description, reservations, currency, priority, facebook, instagram, web, viber, wa, orders, kitchen, logo) VALUES?`, [values], (err, res) => {
                if (err) return reject(err);
                resolve(res);
            });
    });
};

// OCITAVAMO PROFIL OBJEKTA NA USER APP - kasnije dodati evaluations i events i tables prijasnji query je na get_place2

db.get_place = async (id) => {
    console.log("id", id);
    try {
        const menu = await db.get_menu5(id);
        console.log(menu)
        if (menu.length) {
            return new Promise((resolve, reject) => {
                pool.query(`
                SELECT *
                FROM places   
                WHERE id = ?`, [id], (err, res) => {
                    if (err) return reject(err);
                    res[0].menu = menu;
                    return resolve(res);
                });
            });
        }
        else {
            return menu;
        };
    }
    catch (err) {
        console.log(err);
        return (err);
    };
};

db.get_place_status = (id) => {
    console.log(id)
    return new Promise((resolve, reject) => {
        pool.query(`
        SELECT orders, kitchen
        FROM places
        WHERE id=?`, [id], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.update_place_status = (data) => {
    console.log(data)
    const { id, type, status } = data;
    return new Promise((resolve, reject) => {
        pool.query(`
        UPDATE places
        SET ${type} = ?
        WHERE id=?`, [status, id], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};


// MENU

db.get_menu = (id, query) => {
    if (query.column) {
        console.log("categ")
        return new Promise((resolve, reject) => {
            pool.query(`
            SELECT * 
            FROM menu 
            WHERE place_id = ? AND ${query.column} = '${query.value}' AND active = 1`, [id], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    }
    else {
        return new Promise((resolve, reject) => {
            pool.query(`
            SELECT * 
            FROM menu 
            WHERE place_id =? `, [id], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    };
};

db.get_menu5 = (id) => {
    console.log("oooooooo")
    return new Promise((resolve, reject) => {
        pool.query(`
            SELECT * 
            FROM menu 
            WHERE place_id =? AND active = 1`, [id], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.update_menu_item = (data) => {
    const { id, status } = data;
    return new Promise((resolve, reject) => {
        pool.query(`
        UPDATE menu
        SET active = ?
        WHERE item_id=?`, [status, id], (err, res) => {
            if (err) return reject(err);
            resolve(res);
        });
    });
};

db.menu_search_item = (data) => {
    const { id, name } = data;
    return new Promise((resolve, reject) => {
        pool.query(`
            SELECT * 
            FROM menu 
            WHERE place_id =? AND name like '%${name}%'`, [id], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};




// ORDERS

// UPISUJEMO NEW ORDER U BAZU

db.handle_new_order = (data) => {
    const { user_id, place_id, table_no, status, substatus, food_status, message, total, list } = data;
    const order = [[user_id, place_id, table_no, status, substatus, food_status, message, total]];
    return new Promise((resolve, reject) => {
        const execute = async (connection, next) => {
            try {
                const order_id = await db.create_new_order(connection, order);
                await db.update_order_items(connection, order_id, list);
                await db.update_table_status(connection, "busy", place_id, table_no);
                next(null, order_id);
            }
            catch (err) {
                return next(err);
            };
        };

        const end = (err, data) => {
            return err ? reject(err) : resolve(data);
        };

        poolTransaction(pool, execute, end);
    });
};

db.create_new_order = (connection, order) => {
    return new Promise((resolve, reject) => {
        connection.query(`
            INSERT INTO orders
            (user_id, place_id, table_no, status, substatus, food_status, message, total) VALUES ?`, [order], (err, res) => {
            if (err) return reject(err);
            resolve(res.insertId);
        });
    });
};

db.update_order_items = (connection, id, list) => {
    const order_items = QH.handle_order_items(id, list);
    return new Promise((resolve, reject) => {
        connection.query(`
            INSERT INTO order_items 
            (order_id, item_id, quantity, price) VALUES ?`, [order_items], (err) => {
            if (err) return reject(err);
            resolve("ok");
        });
    });
};




// UPDATE order status PV: update_order_status2

db.handle_order_status_update = (data) => {
    const { id, change, place_id, staff_id, table_no } = data;
    const query = QH.update_order_status(change);
    const order_log_action = utils.check_order_log(data);
    const table_status_check = utils.check_table_update(data);

    return new Promise((resolve, reject) => {
        const execute = async (connection, next) => {
            try {
                await db.update_order_status(connection, query, id);
                order_log_action ? await db.update_order_log_table(connection, [[id, staff_id, order_log_action]]) : null;
                table_status_check ? await db.update_table_status(connection, "free", place_id, table_no) : null;
                next(null, place_id);
            }
            catch (err) {
                return next(err);
            };
        };

        const end = (err, data) => {
            return err ? reject(err) : resolve(data);
        };

        poolTransaction(pool, execute, end);
    });
};


db.update_order_status = (connection, query, id) => {
    return new Promise((resolve, reject) => {
        connection.query(`
            UPDATE orders 
            SET ${query} 
            WHERE id =?`, [id], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};


db.handle_order_table_change = (data) => {
    const { place_id, prev_table, new_table, orders } = data;
    const query = QH.update_order_table(orders);
    const log_values = utils.update_order_table(data);

    return new Promise((resolve, reject) => {
        const execute = async (connection, next) => {
            try {
                await db.update_order_table_no(connection, new_table, query);
                await db.update_order_log_table(connection, log_values);
                await db.update_table_status(connection, "free", place_id, prev_table);
                await db.update_table_status(connection, "busy", place_id, new_table);
                next(null, place_id);
            }
            catch (err) {
                return next(err);
            };
        };

        const end = (err, data) => {
            return err ? reject(err) : resolve(data);
        };

        poolTransaction(pool, execute, end);
    });
};

db.update_order_table_no = (connection, table_no, query) => {
    return new Promise((resolve, reject) => {
        connection.query(`
            UPDATE orders 
            SET table_no = ${table_no} 
            WHERE ${query}`, (err) => {
            if (err) return reject(err);
            return resolve(table_no);
        });
    });
};

db.update_order_log_table = (connection, values) => {
    return new Promise((resolve, reject) => {
        connection.query(`
        INSERT INTO order_log 
        (order_id, staff_id, action) VALUES ?`, [values], (err) => {
            if (err) return reject(err);
            return resolve("ok");
        });
    });
};




// PLACE PREUZIMA SVE AKTIVNE NARUDZBE NA PAGE-LOAD I REFRESH PV: get_active_orders2

db.get_active_orders = (id) => {
    return new Promise((resolve, reject) => {
        pool.query(`
            SELECT * FROM orders 
            WHERE status = "active" AND NOT substatus = "review" AND place_id =?`, [id], (err, res) => {
            if (err) return reject(err);

            if (res.length) {
                const order_ids = QH.get_active_orders(res);
                pool.query(`
                    SELECT order_items.*, menu.name, menu.type
                    FROM order_items
                    INNER JOIN menu ON order_items.item_id = menu.item_id
                    WHERE ${order_ids}`, (err, res2) => {
                    if (err) return reject(err);

                    res.forEach(order => {
                        const order_items = res2.filter(items => items.order_id === order.id);
                        order.list = order_items;
                    });
                    return resolve(res);
                });
            }
            else {
                return resolve(res);
            };
        });
    });
};

db.join_order = (data) => {
    const { join_id, join_to_id, total } = data;
    return new Promise((resolve, reject) => {
        const execute = (connection, next) => {
            connection.query(`
            UPDATE orders 
            SET status = 'joined', substatus = ?
            WHERE id = ?`, [join_to_id, join_id], (err) => {
                if (err) return next(err);

                connection.query(`
                UPDATE orders 
                SET total = total + ?
                WHERE id = ?`, [total, join_to_id], (err) => {
                    if (err) return next(err);

                    connection.query(`
                    UPDATE order_items 
                    SET order_id = ?
                    WHERE order_id = ?`, [join_to_id, join_id], (err, res) => {
                        return err ? next(err) : next(null, res.affectedRows);
                    });
                });
            });
        };

        const end = (err, data) => {
            return err ? reject(err) : resolve(data);
        };

        poolTransaction(pool, execute, end);
    });
};

// ORDERS_LOG

db.create_order_log = (data) => {
    const values = [[data.order_id, data.staff_id, data.action]]
    console.log("values", values, data)
    return new Promise((resolve, reject) => {
        pool.query(`
        INSERT INTO order_log 
        (order_id, staff_id, action) VALUES ?`, [values], (err, res) => {
            if (err) return reject(err);
            console.log("Upisano");
            return resolve(res);
        });
    });
};



// STOLOVI

db.update_table_status = (connection, status, place_id, table_no) => {
    return new Promise((resolve, reject) => {
        connection.query(
            `UPDATE tables 
            SET status = ?
            WHERE place_id =? AND table_no =?`, [status, place_id, table_no], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};




// STAFF



db.handle_staff_initial_data_load = async (id) => {
    try {
        const orders = await db.get_active_orders(id);
        const menu = await db.get_menu5(id);
        const tables = await db.get_tables_staff(id);

        return {
            orders,
            menu,
            tables
        };
    }
    catch (err) {
        return err;
    };
};

db.get_tables_staff = (id) => {
    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT *
            FROM tables
            WHERE place_id = ?`, [id], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};




db.get_free_tables = (id) => {
    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT id, table_no
            FROM tables
            WHERE status = ? AND rest_id =?`, ['free', id], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.get_users_orders = (data) => {
    console.log("users_orders", data);
    const { id, status } = data;

    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT orders.*, places.name
            FROM orders 
            INNER JOIN places ON orders.place_id = places.id
            WHERE orders.user_id = '12345' AND orders.status = 'active'
            
            LIMIT 5`, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};




db.get_users_orders5 = (data) => {
    console.log("users_orders", data);
    const { id, status } = data;

    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT orders.*, places.name, 
            FROM ((orders 
            INNER JOIN places ON orders.place_id = places.id)
            INNER JOIN order_items ON orders.id = order_items.order_id)
            WHERE orders.user_id =? AND orders.status =?
            GROUP BY orders.id
            LIMIT 5`, [id, status], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};





db.update_in_table = (data) => {
    return new Promise((resolve, reject) => {
        pool.query(`
            UPDATE orders 
            SET total = total + 3
            WHERE id = '100001' AND SET status = 'joined' WHERE id = '100011'`, (err, res) => {
            if (err) return reject(err);
            console.log("ispravljeno")
            return resolve(res);
        });
    });
};


// PLACE SETTINGS




const tables_query = `INSERT INTO tables (table_no, place_id, place, sector, status) VALUES ?`

db.insert_multiple = (data) => {
    return new Promise((resolve, reject) => {
        pool.query(tables_query, [data], (err, res) => {
            if (err) return reject(err);
            console.log("Upisano")
            return resolve(res);
        });
    });
};

db.insertInTable = (data) => {
    let values;
    if (table.includes("login")) {
        values = [[data.user_name, data.password, data.email]];

        return new Promise((resolve, reject) => {
            pool.query(`INSERT INTO ${table} (user_name, password, email) VALUES ?`, [values], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    }
    else {
        values = [[data.id, data.name, data.email, data.tell, data.city, data.country, data.zip, data.address, data.type, data.description, data.open, data.close, data.logo]];

        return new Promise((resolve, reject) => {
            pool.query(`INSERT INTO ${table} (id, name, email, tell, city, country, zip, address, type, description, open, close, logo) VALUES ?`, [values], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    };
};

db.delete_item = () => {
    return new Promise((resolve, reject) => {
        pool.query(`
        UPDATE orders 
            SET status = 'complited'
            WHERE id = '100061'`, (err, res) => {
            if (err) return reject(err);
            console.log("obrisano")
            return resolve(res);
        });
    });
};

db.get_all = (table, data) => {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM ${table}`, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};






















db.get_filtered_rest = (table, data) => {
    return new Promise((resolve, reject) => {
        let query_handler = {
            join: "INNER",
            condition: `WHERE ${table}.city = ? AND ${table}.type LIKE ? AND tables.status =?`,
            data: [data.city, `%${data.type}%`, "free"]
        };
        if (!data.name) {
            if (data.city) {
                if (data.type) {
                    if (!data.open) {
                        query_handler.join = "LEFT"
                    };
                }
                else {
                    query_handler.condition = `WHERE city = ? AND tables.status = ?`;
                    query_handler.data = [data.city, 'free'];
                    if (!data.open) {
                        query_handler.join = "LEFT"
                    };
                };
            }
            else {
                if (data.type) {
                    query_handler.condition = `WHERE type LIKE ? AND tables.status = ?`;
                    query_handler.data = [`%${data.type}%`, 'free'];
                    if (!data.open) {
                        query_handler.join = "LEFT";
                    };
                }
                else {
                    query_handler.condition = `WHERE tables.status = ?`;
                    query_handler.data = ['free'];
                    if (!data.open) {
                        query_handler.join = "LEFT";
                    };
                };
            };
        }
        else {
            query_handler.join = "LEFT";
            query_handler.condition = `WHERE tables.status = ? AND ${table}.name LIKE ?`;
            query_handler.data = ['free', `%${data.name}%`];
        };

        pool.query(
            `SELECT ${table}.*, COUNT(tables.status) AS tables, sesije.r_id AS status
            FROM ((${table} 
            INNER JOIN tables ON ${table}.id = tables.rest_id)
            ${query_handler.join} JOIN sesije ON ${table}.id = sesije.r_id)
            ${query_handler.condition}
            GROUP BY ${table}.id`, query_handler.data, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.get_filter_data = (city) => {
    let query;
    if (city) {
        query = `SELECT city 
        FROM restaurants_profile 
        WHERE city LIKE '${city}%'
        GROUP BY city`
    }
    else {
        query = `SELECT city FROM restaurants_profile GROUP BY city`
    }
    return new Promise((resolve, reject) => {
        pool.query(query, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.get_place2 = async (id, table) => {
    console.log(id);
    const query_with_tables = `SELECT ${table}.*, COUNT(tables.status) AS tables, sesije.r_id AS status, AVG(evaluation.menu + evaluation.ambience + evaluation.service) / 3 AS eval_avg, COUNT(evaluation.rest_id) AS eval_count
    FROM ((places   
    INNER JOIN tables ON ${table}.id = tables.rest_id)
    LEFT JOIN evaluation ON ${table}.id = evaluation.rest_id)
    LEFT JOIN sesije ON ${table}.id = sesije.r_id)
    
    WHERE ${table}.id = ? AND tables.status =?
    GROUP BY ${table}.id`
    try {
        const menu = await db.get_menu(id, "menu", "");
        console.log(menu)
        if (menu.length) {
            return new Promise((resolve, reject) => {
                pool.query(
                    `SELECT ${table}.*, sesije.r_id AS status, AVG(evaluation.menu + evaluation.ambience + evaluation.service) / 3 AS eval_avg, COUNT(evaluation.rest_id) AS eval_count
                    FROM ((${table}   
                    LEFT JOIN evaluation ON ${table}.id = evaluation.rest_id)
                    LEFT JOIN sesije ON ${table}.id = sesije.r_id)
                    WHERE ${table}.id = ?
                    GROUP BY ${table}.id`, [id], (err, res) => {
                    if (err) return reject(err);
                    res[0].menu = menu;
                    return resolve(res);
                });
            });
        }
        else {
            return menu;
        };
    }
    catch (err) {
        console.log(err);
        return (err);
    };
};

db.check_user = (user_name, table) => {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM ${table} WHERE user_name =?`, [user_name], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};


db.registration = (data, table) => {
    let values;
    if (table.includes("login")) {
        values = [[data.user_name, data.password, data.email]];

        return new Promise((resolve, reject) => {
            pool.query(`INSERT INTO ${table} (user_name, password, email) VALUES ?`, [values], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    }
    else {
        values = [[data.id, data.name, data.email, data.tell, data.city, data.country, data.zip, data.address, data.type, data.description, data.open, data.close, data.logo]];

        return new Promise((resolve, reject) => {
            pool.query(`INSERT INTO ${table} (id, name, email, tell, city, country, zip, address, type, description, open, close, logo) VALUES ?`, [values], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    };
};

db.post = (data, table) => {
    let values;

    values = [[data.user_name, data.password, data.email]];

    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO ${table} (r_name, r_password, r_zip) VALUES ?`, [values], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
}

db.update_menu = (data) => {
    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO menu (place_id, name, img, volume, description, price, type, category, subcategory, subsubcategory, active) VALUES ?`, [data], (err, res) => {
            if (err) return reject(err);
            console.log("Upisano")
            return resolve(res);
        });
    });
};

db.get_menu2 = (id, table, query) => {
    if (query.column) {
        console.log("categ")
        return new Promise((resolve, reject) => {
            pool.query(`SELECT * FROM ${table} WHERE rest_id = ${id} AND ${query.column} = '${query.value}'`, (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    }
    else {
        return new Promise((resolve, reject) => {
            pool.query(`SELECT * FROM ${table} WHERE rest_id =?`, [id], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    };
};

// za pretragu artikla kasnije!!
db.get_menu_items = (table) => {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM ${table} WHERE name LIKE "%chic%"`, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.update_order2 = (data) => {
    const values = [[data.user_id, data.rest_id, data.table_no, data.status, data.substatus, data.message]]
    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO orders (user_id, rest_id, table_no, status, substatus, message) VALUES ?`, [values], (err, res) => {
            if (err) return reject(err);
            console.log("Upisano")
            return resolve(res);
        });
    });
};




db.get_users_orders2 = (id) => {
    console.log("users_orders", id);
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM orders WHERE user_id =?`, [id], (err, res) => {
            if (err) return reject(err);

            if (res.length) {
                let order_ids = "";
                let rest_ids = "";
                for (let i = 0; i < res.length; i++) {
                    if (i !== res.length - 1) {
                        order_ids += `order_items.order_id = ${res[i].id} OR `
                        rest_ids += `id = ${res[i].rest_id} OR `
                    }
                    else {
                        order_ids += `order_items.order_id = ${res[i].id}`
                        rest_ids += `id = ${res[i].rest_id}`
                    };
                };
                pool.query(`SELECT order_items.order_id, order_items.quantity, order_items.price, menu.name
                        FROM order_items
                        INNER JOIN menu ON order_items.item_id = menu.item_id
                        WHERE ${order_ids}`, (err, res2) => {
                    if (err) return reject(err);
                    pool.query(`SELECT SUM(price * quantity) AS total, order_id FROM order_items WHERE ${order_ids} GROUP BY order_id `, (err, res3) => {
                        if (err) return reject(err);

                        pool.query(`SELECT name, id FROM restaurants_profile WHERE ${rest_ids}`, (err, res4) => {
                            if (err) return reject(err);


                            console.log(res4)
                            res.forEach(order => {
                                order.list = res2.filter(items => items.order_id === order.id);
                                order.total = res3.find(total => total.order_id === order.id).total;
                                order.rest_name = res4.find(rest => rest.id === order.rest_id).name;
                            });
                            return resolve(res);
                        });
                    });
                });
            }
            else {
                return resolve(res);
            };
        });
    });
};


db.get_users_orders3 = (data) => {
    console.log("users_orders", data);
    const { id, status } = data;

    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT orders.*, restaurants_profile.name, SUM(order_items.price * order_items.quantity) AS total
            FROM ((orders 
            INNER JOIN restaurants_profile ON orders.rest_id = restaurants_profile.id)
            INNER JOIN order_items ON orders.id = order_items.order_id)
            WHERE orders.user_id =? AND orders.status =?
            GROUP BY orders.id
            LIMIT 5`, [id, status], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};



db.update_order_items2 = (data) => {
    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO order_items (order_id, item_id, quantity, price) VALUES ?`, [data], (err, res) => {
            if (err) return reject(err);
            console.log("Upisano");
            return resolve(res);
        });
    });
};

db.update_visit = (data) => {
    const values = [[data.visitor_id, data.rest_id]];
    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO visits (visitor_id, rest_id) VALUES ?`, [values], (err, res) => {
            if (err) return reject(err);
            console.log("Upisana posjeta")
            return resolve(res);
        });
    });
};

db.get_visits_count = (id) => {
    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT COUNT(visits.rest_id) AS total_visits, restaurants_profile.name AS name
            FROM visits
            INNER JOIN restaurants_profile ON visits.rest_id = restaurants_profile.id
            WHERE visits.rest_id =?`, [id], (err, res) => {
            if (err) return reject(err);
            const date_handler = new Date();
            date_handler.setDate(date_handler.getDate() - 1);
            const date = `${date_handler.getFullYear()}-${date_handler.getMonth() + 1}-${date_handler.getDate()} 23:00:00`;
            pool.query(`SELECT COUNT(rest_id) AS today_visit FROM visits WHERE rest_id = ${id} AND visited >= '${date}'`, (err, res2) => {
                if (err) return reject(err);
                res[0].today_visit = res2[0].today_visit;
                console.log(res2, res);
                return resolve(res);
            });
        });
    });
};



db.get_events = (data) => {
    return new Promise((resolve, reject) => {
        let query_handler = `WHERE restaurants_profile.city = '${data.city}'`;
        if (data.type) {
            query_handler += ` AND events.type_1 = '${data.type}' OR events.type_2 = '${data.type}'`;
        };

        if (data.id) {
            query_handler = `WHERE events.rest_id = ${data.id}`;
        };

        pool.query(
            `SELECT events.*, restaurants_profile.name AS name, restaurants_profile.city AS city
            FROM events
            INNER JOIN restaurants_profile ON restaurants_profile.id = events.rest_id
            ${query_handler}
            ORDER BY CASE 
            WHEN events.priority = 'top' THEN '1'
            WHEN events.priority = 'mid' THEN '2'
            ELSE events.priority END
            LIMIT ${data.limit}
            `, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.update_evaluation = (data) => {
    const values = [[data.user_id, data.rest_id, data.order_id, data.menu, data.ambience, data.service, data.comment]]
    return new Promise((resolve, reject) => {
        pool.query(
            `INSERT INTO evaluation 
            (user_id, rest_id, order_id, menu, ambience, service, comment ) 
            VALUES ?`, [values], (err, res) => {
            if (err) return reject(err);
            console.log("Upisano")
            return resolve(res);
        });
    });
};

db.get_evaluations = (data) => {
    console.log(data)
    let query = "rest_id"
    // if(data.type = "user") query = "user_id";
    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT AVG(menu) AS menu_avg, AVG(ambience) AS ambience_avg, AVG(service) AS service_avg, AVG(menu + service + ambience) / 3 AS review_avg, COUNT(rest_id) AS count
            FROM evaluation
            WHERE ${query} =?`, [data.id], (err, res) => {
            if (err) return reject(err);
            pool.query(
                `SELECT user_id, comment, creation, (menu + service + ambience) / 3 AS review_avg
                FROM evaluation
                WHERE NOT comment = '' AND ${query} =? 
                ORDER BY creation DESC
                LIMIT ${data.limit}`, [data.id], (err, res2) => {
                if (err) return reject(err);

                res[0].comments = res2;

                return resolve(res);
            });
        });
    });
};

db.update_reservation = (data) => {
    let date_handler = `${data.date} ${data.time}`
    console.log("date", date_handler)
    const reservation_values = [[data.user_id, data.rest_id, data.persons, data.status, data.substatus, date_handler, data.event]]
    return new Promise((resolve, reject) => {
        pool.query(
            `INSERT INTO reservations 
            (user_id, rest_id, persons, status, substatus, start, event_id) 
            VALUES ?`, [reservation_values], async (err, res) => {
            if (err) return reject(err);
            if (data.message) {
                data.reservation_id = res.insertId;
                try {
                    db.update_reservation_msg(data);
                }
                catch (err) {
                    return reject(err);
                };
            };
            return resolve(res);
        });
    });
};

db.update_reservation_msg = (data) => {
    if (data.message) {
        const message_values = [[data.reservation_id, data.sender, data.msg_status, data.message]];
        return new Promise((resolve, reject) => {
            pool.query(
                `INSERT INTO reservations_msg 
                (reservation_id, sender, status, message) 
                VALUES ?`, [message_values], (err, res) => {
                if (err) return reject(err);
                pool.query(
                    `SELECT *
                    FROM reservations_msg
                    WHERE reservation_id = ${data.reservation_id}
                    ORDER BY creation`, (err, res2) => {
                    if (err) return reject(err);

                    console.log(res2)
                    return resolve(res2);
                });
            });
        });
    };
};




// db.get_reservations = (data) => {
//     console.log("reservation", data)
//     let query = "reservations.rest_id";
//     let name_query = "user_profile";

//     if (data.type === "user") {
//         query = "reservations.user_id";
//         name_query = "restaurants_profile";
//     };

//     return new Promise((resolve, reject) => {
//         pool.query(
//             `SELECT reservations.*, ${name_query}.name AS name, events.title AS event_name
//             FROM reservations
//             INNER JOIN ${name_query} ON reservations.rest_id = restaurants_profile.id
//             LEFT JOIN events ON reservations.event_id = events.id
//             WHERE ${query} =? AND status =?
//             LIMIT ${data.limit}`, [data.id, data.status], (err, res) => {
//             if (err) return reject(err);

//             if (res.length) {
//                 let reservations_ids = "";

//                 for (let i = 0; i < res.length; i++) {
//                     if (i !== res.length - 1) {
//                         reservations_ids += `reservation_id = ${res[i].id} OR `
//                     }
//                     else {
//                         reservations_ids += `reservation_id = ${res[i].id}`
//                     };
//                 };
//                 pool.query(
//                     `SELECT *
//                     FROM reservations_msg
//                     WHERE ${reservations_ids}
//                     ORDER BY creation`, (err, res2) => {
//                     if (err) return reject(err);

//                     res.forEach(reservation => {
//                         const order_items = res2.filter(items => items.reservation_id === reservation.id);
//                         reservation.messages = order_items;
//                     });

//                     return resolve(res);
//                 });
//             }
//             else {
//                 return resolve(res);
//             };
//         });
//     });
// };


db.user_registration = (data) => {
    console.log(data)
    let values = [[data.user_name, data.password, data.email, data.phone, data.birthday, data.city, data.country, data.zip, data.address, data.sex, data.language]];

    return new Promise((resolve, reject) => {
        pool.query(`INSERT INTO user_profile (user_name, password, email, phone, birthday, city, country, zip, address, sex, language) VALUES ?`, [values], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};


module.exports = db

// module.exports.pool = pool.getConnection;

