
const QH = {};

QH.get_active_orders = (data) => {
    let query = "";
    for (let i = 0; i < data.length; i++) {
        if (i !== data.length - 1) {
            query += `order_items.order_id = ${data[i].id} OR `
        }
        else {
            query += `order_items.order_id = ${data[i].id}`
        };
    };

    return query;
};

QH.update_order_status = (data) => {
    let counter = 1;
    let query = "";
    const length = Object.keys(data).length;

    for (let [property, value] of Object.entries(data)) {
        if (length > 1) {
            if (counter === 1) {
                query += `${property} = '${value}',`;
            }
            else if (counter === length) {
                query += ` ${property} = '${value}'`;
            }
            else {
                query += ` ${property} = '${value}',`;
            };
            counter++;
        }
        else {
            query += `${property} = '${value}'`;
        };
    };
    return query;
};

QH.handle_order_items = (id, data) => {
    let query = [];
    console.log(data)
    data.forEach(item => {
        query = [...query, [id, item.item_id, item.quantity, item.price]];
    });
    return query;
};

QH.update_order_table = (data) => {
    let query = "";

    if (data.length > 1) {
        for( let i = 0; i < data.length; i++) {
            if (i !== data.length - 1) {
                query += `id = ${data[i]} OR `;
            }
            else {
                query += `id = ${data[i]}`;
            };
        };

        return query;
    }
    else return `id = ${data[0]}`
};


module.exports = QH