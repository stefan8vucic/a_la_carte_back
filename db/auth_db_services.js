const mySql = require("mysql");
const { env } = require("process");
require("dotenv/config");
const poolTransaction = require('./pool_transaction');

const pool = mySql.createPool({
    connectionLimit: 100,
    user: process.env.AUTH_DB_USER,
    password: process.env.AUTH_DB_PASS,
    host: process.env.AUTH_DB_HOST,
    database: process.env.AUTH_DB_DB,
    port: 3306
});


// NOVO

const create_admin = "CREATE TABLE admin ( id INT AUTO_INCREMENT PRIMARY KEY, user_name VARCHAR(50), password VARCHAR(200), email VARCHAR(100), phone VARCHAR(10), last_login TIMESTAMP, creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=100000"

const create_pos_login = "CREATE TABLE pos ( id INT AUTO_INCREMENT PRIMARY KEY, place_id INT, user_name VARCHAR(50), password VARCHAR(200), type VARCHAR(10), last_login TIMESTAMP, creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=100000"

const create_kitchen_login = "CREATE TABLE kitchen ( id INT AUTO_INCREMENT PRIMARY KEY, place_id INT, user_name VARCHAR(50), password VARCHAR(200), type VARCHAR(10), last_login TIMESTAMP, creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=100000"

const create_user_login2 = "CREATE TABLE users ( id INT AUTO_INCREMENT PRIMARY KEY, user_name VARCHAR(30), password VARCHAR(200), email VARCHAR(100), phone VARCHAR(50), birthday DATE, city VARCHAR(50), zip INT, address VARCHAR(255), country VARCHAR(50), sex VARCHAR(5), language VARCHAR(50), last_login TIMESTAMP, creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP ) ENGINE=InnoDB AUTO_INCREMENT=2000000";

const create_user_session = "CREATE TABLE user_session (id INT, socket_id VARCHAR(255), token VARCHAR(255))"

const create_sector_session = "CREATE TABLE place_session (place_id INT, socket_id VARCHAR(255), token VARCHAR(255))"

const create_place_login = "CREATE TABLE place_login ( id INT AUTO_INCREMENT PRIMARY KEY, place_id INT, user_name VARCHAR(50), password VARCHAR(200), pos BIT, kitchen BIT, staff BIT, events BIT, creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=100000"

const create_place_session_log = "CREATE TABLE session_log (id INT AUTO_INCREMENT PRIMARY KEY, place_id INT, staff_id INT, start TIMESTAMP DEFAULT CURRENT_TIMESTAMP, end TIMESTAMP DEFAULT 0 ON UPDATE CURRENT_TIMESTAMP) ENGINE=InnoDB AUTO_INCREMENT=100000"


const delete_table = "DROP TABLE place_session"

const db = {}

db.delete_table = () => {
    return new Promise((resolve, reject) => {
        pool.query(delete_table, (err, res) => {
            if (err) return reject(err);
            console.log("obrisano")
            return resolve(res);
        });
    });
};

db.create_table = () => {
    return new Promise((resolve, reject) => {
        pool.query(create_sector_session, (err, res) => {
            if (err) return reject(err);
            console.log("kreirano")
            return resolve(res);
        });
    });
};


// NOVO

// registrujemo novi profil(pos, kuhinja, staff, emenu)

db.place_registration = (data) => {
    let values = [[data.place_id, data.user_name, data.password, data.pos, data.kitchen, data.staff, data.events]];
    return new Promise((resolve, reject) => {
        pool.query(
            `INSERT INTO place_login (place_id, user_name, password, pos, kitchen, staff, events) VALUES?`, [values], (err, res) => {
                if (err) return reject(err);
                console.log("upisano");
                resolve(res);
            });
    });
};

// provjeravamo da li postoji user_name za login

db.check_place_login = (user_name) => {
    return new Promise((resolve, reject) => {
        pool.query(`
            SELECT * 
            FROM place_login 
            WHERE user_name =?`, [user_name], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

// nakon sto se neki sektor objekta uspjesno loguje upisujemo njegovu sesiju u bazu
// SOCKET SESSIONS

db.create_session_log = (place_id, id) => {
    const session_log = [[place_id, id]];
    return new Promise((resolve, reject) => {
        pool.query(`
                INSERT INTO session_log 
                (place_id, staff_id) VALUES ?`, [session_log], (err, res) => {
            if (err) return reject(err);
            return resolve(res.insertId);

        });
    });
};

db.create_session = (place_id, r_token) => {
    const session = [[place_id, r_token]];
    return new Promise((resolve, reject) => {
        pool.query(`
        INSERT INTO place_session 
        (place_id, token) VALUES ?`, [session], (err, res) => {
            if (err) return reject(err);
            return resolve(res.insertId);
        });
    });
};

// db.logIn = (data) => {
//     const session = [[data.place_id, data.r_token]];
//     const session_log = [[data.place_id, data.id]];
//     return new Promise((resolve, reject) => {
//         const execute = (connection, next) => {
//             connection.query(`
//             INSERT INTO place_session (place_id, token) VALUES ?`, [session], (err) => {
//                 if (err) return next(err);

//                 connection.query(`
//                 INSERT INTO session_log (place_id, staff_id) VALUES ?`, [session_log], (err, res) => {
//                     return err ? next(err) : next(null, res.insertId);
//                 });
//             });
//         };

//         const end = (err, data) => {
//             return err ? reject(err) : resolve(data);
//         };

//         poolTransaction(pool, execute, end);
//     });
// };

db.session_check = (token) => {
    return new Promise((resolve, reject) => {
        pool.query(`
            SELECT *
            FROM place_session
            WHERE token = ?`, [token], (err, res) => {
            if (err) return reject(err);
            if (!res.length) return reject("No session");

            return resolve(res);
        });
    });
};







db.get_session_logs = () => {
    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT session_log.*, place_login.user_name
            FROM session_log 
            INNER JOIN place_login ON place_login.id = session_log.staff_id `, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};



db.update_session_token = (prev_token, new_token) => {
    return new Promise((resolve, reject) => {
        pool.query(`
            UPDATE place_session 
            SET token = ?
            WHERE token = ?`, [new_token, prev_token], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

// NA SOCKET CONNECTION UPISUJEMO SOCKET ID
db.update_place_session = (data) => {
    const { token, socket_id, place_id } = data;
    return new Promise((resolve, reject) => {

        pool.query(`
        SELECT *
        FROM place_session
        WHERE token = ?`, [token], (err, res) => {
            if (err) return reject(err);

            if (res.length) {
                pool.query(`
                    UPDATE place_session 
                    SET socket_id = ?
                    WHERE token = ?`, [socket_id, token], (err, res) => {
                    if (err) return reject(err);
                    return resolve(res);
                });
            }
            else {
                const values = [[place_id, socket_id, token]];
                pool.query(`
                    INSERT INTO place_session 
                    (place_id, socket_id, token) VALUES ?`, [values], (err, res) => {
                    if (err) return reject(err);
                    return resolve(res);
                });
            };
        });
    });
};

db.delete_place_session = (token) => {
    return new Promise((resolve, reject) => {
        pool.query(
            `DELETE FROM place_session 
            WHERE token = ?`, [token], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.update_session_log = (data) => {
    const { session_id } = data;
    return new Promise((resolve, reject) => {
        pool.query(`
        UPDATE session_log 
        SET end = CURRENT_TIMESTAMP
        WHERE id = ?`, [session_id], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};


db.check_place_session = (token) => {
    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT * FROM place_session 
            WHERE token LIKE '%${token}'`, (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};

db.get_place_sockets = (id) => {

    return new Promise((resolve, reject) => {
        pool.query(
            `SELECT socket_id
            FROM place_session 
            WHERE place_id = ?`, [id], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};














// db.insert_multiple = (data) => {
//     return new Promise((resolve, reject) => {
//         pool.query(user_query, [data], (err, res) => {
//             if (err) return reject(err);
//             console.log("Upisano")
//             return resolve(res);
//         });
//     });
// };

db.insertInTable = (data) => {
    let values;
    if (table.includes("login")) {
        values = [[data.user_name, data.password, data.email]];

        return new Promise((resolve, reject) => {
            pool.query(`INSERT INTO ${table} (user_name, password, email) VALUES ?`, [values], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    }
    else {
        values = [[data.id, data.name, data.email, data.tell, data.city, data.country, data.zip, data.address, data.type, data.description, data.open, data.close, data.logo]];

        return new Promise((resolve, reject) => {
            pool.query(`INSERT INTO ${table} (id, name, email, tell, city, country, zip, address, type, description, open, close, logo) VALUES ?`, [values], (err, res) => {
                if (err) return reject(err);
                return resolve(res);
            });
        });
    };
};

db.delete_item = () => {
    return new Promise((resolve, reject) => {
        pool.query(`DELETE FROM place_session WHERE place_id = 100001`, (err, res) => {
            if (err) return reject(err);
            console.log("obrisano")
            return resolve(res);
        });
    });
};




db.check_user = (user_name, table) => {
    return new Promise((resolve, reject) => {
        pool.query(`SELECT * FROM ${table} WHERE user_name =?`, [user_name], (err, res) => {
            if (err) return reject(err);
            return resolve(res);
        });
    });
};





module.exports = db


